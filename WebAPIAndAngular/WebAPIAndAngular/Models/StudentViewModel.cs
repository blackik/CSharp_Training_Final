﻿using System.ComponentModel.DataAnnotations;

namespace WebAPIAndAngular.Models
{
    public class StudentViewModel
    {
        public int Id { get; set; }
       
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [Range(0, 100)]
        public int Rate { get; set; }

        [Required]
        public string University { get; set; }
    }
}