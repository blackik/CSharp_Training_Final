﻿app.controller('APIController', function ($scope, APIService) {

    getAll();

    function getAll() {
        var fun = APIService.getStudents();
        fun.then(function (result) {
            $scope.students = result;
        }, function (error) {
            alert("Invalid request");
            console.log('Oops! Something went wrong while fetching the data.');
        });
    }

    $scope.showDetails = function (id) {
        var fun = APIService.getStudent(id);
        fun.then(function (result) {
                $scope.studentDetials = result;
            },
            function (error) {
                alert("Invalid request");
                console.log('Oops! Something went wrong while retriving the data.');
            });
    };

    $scope.saveStudent = function () {
        var student = {
            FirstName: $scope.FirstName,
            LastName: $scope.LastName,
            Rate: $scope.Rate,
            University: $scope.University
        };
        var fun = APIService.saveStudent(student);
        fun.then(function () {
            getAll();

            $scope.FirstName = "";
            $scope.LastName = "";
            $scope.Rate = "";
            $scope.University = "";

            },
            function (error) {
                alert("Invalid request");
                console.log('Oops! Something went wrong while saving the data.');
            });
    };    
   
    $scope.deleteStudent = function (id) {
        var fun = APIService.deleteStudent(id);
        fun.then(function () {
                getAll();
            },
            function (error) {
                alert("Invalid request");
                console.log('Oops! Something went wrong while deleting the data.');
            });
    };
});