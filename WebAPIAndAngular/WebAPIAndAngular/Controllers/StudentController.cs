﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebAPIAndAngular.Filters;
using WebAPIAndAngular.Models;

namespace WebAPIAndAngular.Controllers
{
    public class StudentController : ApiController
    {
        private List<StudentViewModel> _studentList;
        public StudentController()
        {
            PrepareData();
        }

        [Route("api/Student")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            var result = _studentList.Select(s => new { s.Id, s.FirstName, s.LastName });
            return Ok(result);
        }

        [Route("api/Student/{studentId}")]
        [HttpGet]
        public IHttpActionResult Get(int studentId)
        {
            var student = _studentList.Single(s => s.Id == studentId);
            return Ok(new { student.Rate, student.University });
        }

        [ValidationFilter]
        [Route("api/Student")]
        [HttpPost]
        public IHttpActionResult Post(StudentViewModel studentViewModel)
        {
          
                var newId = _studentList.Max(s => s.Id) + 1;
                studentViewModel.Id = newId;
                _studentList.Add(studentViewModel);
                return Ok(newId);          
        }

        [Route("api/Student/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var studentToDelete = _studentList.FirstOrDefault(s => s.Id == id);
            if (studentToDelete == null)
            {
                return NotFound();
            }
            _studentList.Remove(studentToDelete);
            return Ok();
        }

        private void PrepareData()
        {
            const string sessionListName = "StudentList";

            if (HttpContext.Current.Session[sessionListName] == null)
            {
                _studentList = new List<StudentViewModel>()
                {
                    new StudentViewModel()
                    {
                        Id = 1,
                        FirstName = "Robert",
                        LastName = "Lewandowski",
                        Rate = 3,
                        University = "Politechnika Śląska"
                    },
                    new StudentViewModel()
                    {
                        Id = 2,
                        FirstName = "Turbo",
                        LastName = "Grosik",
                        Rate = 5,
                        University = "Uniwersytet Śląski"
                    },
                    new StudentViewModel()
                    {
                        Id = 3,
                        FirstName = "Jakub",
                        LastName = "Błaszczykowski",
                        Rate = 4,
                        University = "AGH"
                    }
                };

                HttpContext.Current.Session[sessionListName] = _studentList;
            }
            else
            {
                _studentList = (List<StudentViewModel>)HttpContext.Current.Session["StudentList"];
            }
        }
    }
}
